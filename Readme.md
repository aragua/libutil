meson setup build
meson compile -C build
DESTDIR=$PWD/test meson install -C build
clang-format -style=file -i *.c *.h