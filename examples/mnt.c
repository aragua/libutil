#include <mount.h>
#include <log.h>

int main(int argc, char **argv)
{
    if ((argc > 0) && argv[1])
    {
        int ret = is_mounted(argv[1]);
        LOG_INFO("%s is %sa mountpoint\n", argv[1], ret ? "" : "not ");
        return ret;
    }
    else
    {
        LOG_ERR("Invalid arguments\n");
        return -1;
    }
}