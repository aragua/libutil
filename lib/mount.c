#include <stdio.h>
#include <mntent.h>
#include <errno.h>
#include <string.h>

#include "mount.h"

#include "log.h"

int is_mounted(const char *mountpoint)
{
    int found = 0;

    if (mountpoint)
    {
        FILE * fp = setmntent("/proc/mounts", "r");
        if (fp)
        {
            struct mntent ent;
            char buf[1024];

            while (getmntent_r(fp, &ent, buf, sizeof(buf)))
            {
                if (strcmp(mountpoint, ent.mnt_dir) == 0)
                {
                    found = 1;
                    break;
                }
            }
        }
        else
        {
            LOG_ERR("Failed to set mnt entry: %s", strerror(errno));
            found = -1;
        }
    }
    else
    {
        LOG_ERR("Failed to read /proc/mounts: %s", strerror(errno));
        found = -1;
    }
    return found;
}