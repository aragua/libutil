#include <fcntl.h>
#include <libgen.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "log.h"

#include "file.h"

/* return NULL on failure else size of content */
u_int8_t *file_get_content(char *filename, unsigned long *size)
{
	u_int8_t *content = NULL;
	struct stat st;

	if (!filename || !size)
		return NULL;

	if (stat(filename, &st) == 0)
	{
		*size = st.st_size;
		content = malloc(*size);
		if (content)
		{
			int filefd = open(filename, O_RDONLY);
			if (filefd >= 0)
			{
				int rsz = read(filefd, content, *size);
				if (rsz != *size)
				{
					free(content);
					content = NULL;
				}
				close(filefd);
			}
		}
	}
	return content;
}

ssize_t file_set_content(char *filename, size_t size, u_int8_t *content)
{
	int filefd;
	ssize_t wsz = -1;
	char *dir;

	if (!filename || !content || size == 0)
	{
		LOG_ERR("Invalide arguments");
		return -1;
	}
	dir = strdup(filename);
	dir = dirname(dir);
	if (access(dir, F_OK) != 0)
	{
		LOG_ERR("Missing directory to copy %s\n", filename);
		free(dir);
	}
	else
	{
		free(dir);
		filefd = open(filename, O_WRONLY | O_CREAT, 0600);
		if (filefd >= 0)
		{
			wsz = write(filefd, content, size);
			close(filefd);
		}
	}
	return wsz;
}