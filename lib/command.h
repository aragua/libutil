#ifndef UTILS_COMMAND_H
#define UTILS_COMMAND_H

char *system_cmd(char *cmd, int *ret);
int system_cmd_v2(char **result, char *cmd_fmt, ...);

#endif