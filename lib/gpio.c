#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "utils.h"

#include "gpio.h"

int gpio_export(u_int32_t nbr)
{
	char value_path[4096];
	char gpio_nbr[8];
	int gpio_fd, ret = 0;

	format_string(gpio_nbr, 8, "%u", nbr);
	format_string(value_path, 4096, "/sys/class/gpio/gpio%s/value", gpio_nbr);

	/* If gpio not exported, export it */
	if (access(value_path, W_OK) != 0)
	{
		gpio_fd = open("/sys/class/gpio/export", O_WRONLY);
		if (gpio_fd >= 0)
		{
			if (write(gpio_fd, gpio_nbr, strlen(gpio_nbr)) == strlen(gpio_nbr))
			{
				ret = 0;
			}
			else
			{
				ret = -1;
			}
			close(gpio_fd);
		}
		else
		{
			ret = -1;
		}
	}
	return ret;
}

int gpio_unexport(u_int32_t nbr)
{
	char value_path[4096];
	char gpio_nbr[8];
	int gpio_fd, ret = 0;

	format_string(gpio_nbr, 8, "%u", nbr);
	format_string(value_path, 4096, "/sys/class/gpio/gpio%s/value", gpio_nbr);

	/* If gpio exported, unexport it */
	if (access(value_path, W_OK) == 0)
	{
		gpio_fd = open("/sys/class/gpio/unexport", O_WRONLY);
		if (gpio_fd >= 0)
		{
			if (write(gpio_fd, gpio_nbr, strlen(gpio_nbr)) == strlen(gpio_nbr))
			{
				ret = 0;
			}
			else
			{
				ret = -1;
			}
			close(gpio_fd);
		}
		else
		{
			ret = -1;
		}
	}
	return ret;
}

int gpio_set_value(u_int32_t nbr, char value)
{
	int dir_fd, ret = -1;
	char val_path[4096];
	char dir_path[4096];

	format_string(val_path, 4096, "/sys/class/gpio/gpio%u/value", nbr);
	format_string(dir_path, 4096, "/sys/class/gpio/gpio%u/direction", nbr);

	dir_fd = open(dir_path, O_WRONLY);
	if (dir_fd >= 0)
	{
		if (write(dir_fd, "out", 3) == 3)
		{
			int val_fd = open(val_path, O_WRONLY);
			if (val_fd >= 0)
			{
				if (write(val_fd, &value, 1) == 1)
				{
					ret = 0;
				}
				close(val_fd);
			}
		}
		close(dir_fd);
	}
	return ret;
}

int gpio_get_value(u_int32_t nbr, char *value)
{
	int dir_fd, ret = -1;
	char val_path[4096];
	char dir_path[4096];

	format_string(val_path, 4096, "/sys/class/gpio/gpio%u/value", nbr);
	format_string(dir_path, 4096, "/sys/class/gpio/gpio%u/direction", nbr);

	dir_fd = open(dir_path, O_WRONLY);
	if (dir_fd >= 0)
	{
		if (write(dir_fd, "in", 2) == 2)
		{
			int val_fd = open(val_path, O_RDONLY);
			if (val_fd >= 0)
			{
				if (read(val_fd, value, 1) == 1)
				{
					ret = 0;
				}
				close(val_fd);
			}
		}
		close(dir_fd);
	}
	return ret;
}