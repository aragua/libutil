#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <dirent.h>

#include "log.h"
#include "command.h"

#define MAX_PATH_LENGTH 4096
/*
 * Return 0 if directory exists, number of directory created or -1 on error
 */
int recursive_mkdir(const char *path, int mode)
{
	int ret = -1;

	if (path)
	{
		if (access(path, F_OK) == 0)
		{
			ret = 0;
		}
		else
		{
			char rpath[MAX_PATH_LENGTH];
			int len;

			if (realpath(path, rpath))
			{
				for (char *p = rpath + 1; *p != 0; p++)
				{
					if (*p == '/')
					{
						*p = 0;
						if (access(rpath, F_OK) != 0)
						{
							if (mkdir(rpath, 0777) != 0)
							{
								LOG_ERR("Error creating directory %s : %s\n", rpath, strerror(errno));
								ret = -1;
								break;
							}
							else
							{
								ret++;
							}
						}
						*p = '/';
					}
				}
			}
			if ((ret >= 0) && mkdir(path, 0777) == 0)
			{
				ret++;
			}
			else
			{
				LOG_ERR("Error creating directory %s: %s\n", path, strerror(errno));
				ret = -1;
			}
		}
	}
	return ret;
}

int create_ramdisk(const char *mp, unsigned int size_in_gb)
{
	char options[256];

	if (!mp || access(mp, F_OK) != 0 || (system_cmd_v2(NULL, "mount | grep -q %s", mp) == 0))
	{
		LOG_ERR("Cannot access ram disk mount point or already mounted: %s\n", mp);
		return -1;
	}
	if (size_in_gb < 50 || size_in_gb > 1024)
	{
		LOG_ERR("Ram disk size over limit: %u\n", size_in_gb);
		return -1;
	}
	snprintf(options, 256, "size=%uM,uid=0,gid=0,mode=700", size_in_gb);
	return mount("tmpfs", mp, "tmpfs", 0, options);
}


int file_copy(char *from, char *to)
{
    char buffer[1024];
    int ffrom;
    int fto;
    int ret = -1;
    ssize_t count;

    /* Check for insufficient parameters */
    if (!from || !to)
        return -1;

    if (access(to,F_OK) == 0)
    {
        LOG_DBG("File %s exists\n", to);
        if (unlink(to) != 0)
        {
            LOG_ERR("Fail to unlink %s: %s\n", to, strerror(errno));
            return -1;
        }
    }

    ffrom = open(from, O_RDONLY);
    if (ffrom >= 0)
    {
        fto = open(to, O_WRONLY | O_CREAT, 0666);
        if (fto >= 0)
        {
            while ((count = read(ffrom, buffer, sizeof(buffer))) != 0)
                write(fto, buffer, count);
            ret = 0;
            close(fto);
        }
        else
        {
            LOG_ERR("Fail to open %s: %s\n", to, strerror(errno));
        }
        close(ffrom);
    }
    else
    {
        LOG_ERR("Fail to open %s: %s\n", from, strerror(errno));
    }
    return ret;
}

int file_move(char *from, char *to)
{
    if (from && to)
    {
        if (file_copy(from, to) == 0)
        {
            if (unlink(from) == 0)
            {
                return 0;
            }
            else
            {
                LOG_ERR("Fail to unlink %s: %s\n", from, strerror(errno));
            }
        }
        else
        {
            LOG_ERR("File_copy fail\n");
        }
    }
    else
    {
        LOG_ERR("Invalid parameters from:%s to:%s\n", from, to);
    }
    return -1;
}

int count_files_in_directory(const char *path)
{
    DIR *dir_ptr = NULL;
    struct dirent *direntp;

    if (!path)
        return 0;
    
    if( (dir_ptr = opendir(path)) == NULL )
    {
        LOG_ERR("Fail to open directory %s: %s\n", path, strerror(errno));
        return 0;
    }

    int count=0;
    while( (direntp = readdir(dir_ptr)))
    {
        if (strcmp(direntp->d_name,".")==0 ||
            strcmp(direntp->d_name,"..")==0)
        {
            continue;
        }
        switch (direntp->d_type)
        {
            case DT_REG:
                ++count;
                break;
        }
    }
    closedir(dir_ptr);
    return count;
}