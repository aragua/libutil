#ifndef UTILS_GPIO_H
#define UTILS_GPIO_H

int gpio_export(u_int32_t nbr);
int gpio_unexport(u_int32_t nbr);
int gpio_set_value(u_int32_t nbr, char value);
int gpio_get_value(u_int32_t nbr, char *value);

#endif