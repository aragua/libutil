#ifndef UTILS_FILESYSTEM_H
#define UTILS_FILESYSTEM_H

int recursive_mkdir(const char *path, int mode);
int create_ramdisk(const char *mp, unsigned int size_in_gb);
int count_files_in_directory(const char *path);
int file_copy(char *from, char *to);
int file_move(char *from, char *to);

#endif