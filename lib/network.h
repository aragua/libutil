#ifndef UTILS_NETWORK_H
#define UTILS_NETWORK_H

char *net_get_ip_addr(char *ifname);
int net_iface_up_and_running(char *ifname);
char *net_get_mac_addr(char *ifname);
void net_show_ip(const char *name, struct in_addr addr);

#endif