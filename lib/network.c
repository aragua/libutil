
#define _GNU_SOURCE
#include <arpa/inet.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include "log.h"
#include "network.h"
#include "utils.h"

char *net_get_ip_addr(char *ifname)
{
	char *ret = NULL;
	int fd;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd >= 0)
	{
		struct ifreq ifr;
		ifr.ifr_addr.sa_family = AF_INET;
		strncpy(ifr.ifr_name, ifname, IFNAMSIZ - 1);
		if (ioctl(fd, SIOCGIFADDR, &ifr) == 0)
		{
			asprintf(&ret, "%s", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
		}
		close(fd);
	}
	return ret;
}

int net_iface_up_and_running(char *ifname)
{
	int fd, ret = -1;
	struct ifreq ifr;

	fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd >= 0)
	{
		memset(&ifr, 0, sizeof(ifr));
		strncpy(ifr.ifr_name, ifname, IFNAMSIZ - 1);

		ret = ioctl(fd, SIOCGIFFLAGS, &ifr);
		if (ret >= 0)
		{
			if (ifr.ifr_flags & (IFF_UP | IFF_RUNNING))
				ret = 1;
			else
				ret = 0;
		}
		close(fd);
	}
	return ret;
}

char *net_get_mac_addr(char *ifname)
{
	char *ret = NULL;

	if (ifname)
	{
		int fd = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd >= 0)
		{
			struct ifreq ifr;
			ifr.ifr_addr.sa_family = AF_INET;
			strncpy(ifr.ifr_name, ifname, IFNAMSIZ - 1);
			if (ioctl(fd, SIOCGIFHWADDR, &ifr) == 0)
			{
				if (ifr.ifr_hwaddr.sa_family == ARPHRD_ETHER)
				{
					const unsigned char *mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
					asprintf(&ret, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
				}
			}
			close(fd);
		}
	}
	return ret;
}

void net_show_ip(const char *name, struct in_addr addr)
{
	if (name)
		LOG_INFO("%s: %s\n", name, inet_ntoa(addr));
}