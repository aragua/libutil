#ifndef UTILS_FILE_H
#define UTILS_FILE_H

#include <sys/types.h>

u_int8_t *file_get_content(char *filename, unsigned long *size);
ssize_t file_set_content(char *filename, size_t size, u_int8_t *content);

#endif