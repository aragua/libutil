#ifndef UTILS_H
#define UTILS_H

#include "file.h"
#include "gpio.h"
#include "log.h"

#include <fcntl.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define format_string(str, sz, format, ...)                                                                            \
	{                                                                                                                  \
		if (snprintf(str, sz, format, __VA_ARGS__) < 0)                                                                \
			return (-1);                                                                                               \
	}

#define ptr_destroy(ptr)                                                                                               \
	{                                                                                                                  \
		if (ptr)                                                                                                       \
			free(ptr);                                                                                                 \
		ptr = NULL;                                                                                                    \
	}

#endif