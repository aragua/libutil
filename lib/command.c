#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <stdarg.h>

#define BUFFER_MAX_SZ 131072
char *system_cmd(char *cmd, int *ret)
{
	FILE *fp;
	size_t count = 0;
	char *buffer = malloc(BUFFER_MAX_SZ);

	if (!cmd || !ret)
		return NULL;

	if (buffer)
	{
		char tmp[4096];

		fp = popen(cmd, "r");
		if (fp == NULL)
		{
			printf("Failed to run command %s\n", cmd);
			free(buffer);
			return NULL;
		}
		while (fgets(tmp, sizeof(tmp), fp) != NULL)
		{
			int len = strlen(tmp);
			if (len + count <= BUFFER_MAX_SZ)
			{
				memcpy(buffer + count, tmp, len + 1);
				count += len;
			}
		}
		*ret = pclose(fp);
	}
	return buffer;
}

int system_cmd_v2(char **result, char *cmd_fmt, ...)
{
	int ret = -1;
	va_list args;
	char cmd[8192];

	if (!cmd_fmt)
		return -1;

	va_start(args, cmd_fmt);
	ret = vsnprintf(cmd, 8192, cmd_fmt, args);
	va_end(args);

	if (ret >= 0 && ret < 8192) // the command is created and not truncated
	{
		char *buffer = malloc(BUFFER_MAX_SZ);
		if (buffer)
		{
			int count = 0;
			FILE *fp;

			fp = popen(cmd, "r");
			if (fp)
			{
				while (fgets(buffer + count, sizeof(buffer) - count, fp) != NULL)
				{
					count = strlen(buffer);
					if (count > BUFFER_MAX_SZ)
						break;
				}
				if (result)
					*result = buffer;
				else
					free(buffer);
				ret = pclose(fp);
				return count;
			}
			else
			{
				free(buffer);
			}
		}
	}
	return -1;
}
