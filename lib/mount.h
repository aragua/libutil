#ifndef UTIL_MOUNT_H
#define UTIL_MOUNT_H

int is_mounted(const char *mountpoint);

#endif