#ifndef UTILS_LOG_H
#define UTILS_LOG_H

#include <stdio.h>

extern int LOG_verbose;

void log_set_level(int level);

#define LOG_DBG(fmt, ...)                                                                                              \
	{                                                                                                                  \
		if (LOG_verbose)                                                                                               \
			fprintf(stderr, "%s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__);                                         \
	}

#define LOG_INFO(fmt, ...)                                                                                             \
	{                                                                                                                  \
		fprintf(stderr, fmt, ##__VA_ARGS__);                                                                           \
	}

#define LOG_WARN(fmt, ...)                                                                                             \
	{                                                                                                                  \
		fprintf(stderr, "%s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__);                                             \
	}

#define LOG_ERR(fmt, ...)                                                                                              \
	{                                                                                                                  \
		fprintf(stderr, "%s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__);                                             \
	}

#define LOG_CRIT(fmt, ...)                                                                                             \
	{                                                                                                                  \
		fprintf(stderr, "%s:%d: " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__);                                        \
		exit(-1);                                                                                                      \
	}

#endif